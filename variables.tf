variable "access_key" {
  type        = string
  description = "Enter your access key for AWS concole"
}
variable "secret_key" {
  type        = string
  description = "Enter your secret key for AWS concole"
}
variable "ssh_key" {
  type        = string
  description = "Enter your ssh public key for connect to instances"
}
variable "region" {
  description = "Enter AWS Region to deploy server"
  type        = string
  default     = "eu-central-1"
}
variable "instance_type" {
  description = "Enter AWS Instance type"
  type        = string
  default     = "t2.micro"
}
variable "allow_ports" {
  description = "List of ports to open for server"
  type        = map(any)
  default = {
    80  = ["0.0.0.0/0"],
    443 = ["0.0.0.0/0"],
  }
}
variable "common_tags" {
  description = "Common tags apply to all resources"
  type        = map(any)
  default = {
    Owner       = "Dmitriy Smirnov"
    Project     = "Lear2DevOps"
    Environment = "Devopment"
  }
}
variable "detailed_monitoring" {
  description = "Enable detailed monitoring? ()"
  type        = bool
  default     = false
}
