provider "aws" {
  access_key = var.access_key
  secret_key = var.secret_key
  region     = var.region
}

resource "aws_security_group" "WebServerSG" {
  name        = "WebServer Security Group"
  description = "Security Group"
  tags = merge(var.common_tags, {
    "Name" = "${var.common_tags["Environment"]} Security group of WebServer"
  })
  dynamic "ingress" {
    for_each = var.allow_ports
    content {
      cidr_blocks      = ingress.value
      description      = "${var.common_tags["Environment"]} WSG ingress for ${ingress.key} port"
      from_port        = ingress.key
      ipv6_cidr_blocks = ["::/0"]
      prefix_list_ids  = []
      protocol         = "tcp"
      security_groups  = []
      self             = false
      to_port          = ingress.key
    }
  }
  egress = [{
    from_port        = 0
    to_port          = 0
    cidr_blocks      = ["0.0.0.0/0"]
    protocol         = "-1"
    description      = "${var.common_tags["Environment"]} WSG egress for all port"
    ipv6_cidr_blocks = ["::/0"]
    prefix_list_ids  = []
    security_groups  = []
    self             = false
  }]
}

resource "aws_launch_configuration" "WebServerLC" {
  name_prefix     = "WebServer-LC-"
  image_id        = data.aws_ami.ubuntu_server_ami.id
  instance_type   = var.instance_type
  security_groups = [aws_security_group.WebServerSG.id]
  user_data       = file("bash_scripts/run_webserver.sh")
  key_name        = aws_key_pair.ssh_key.key_name
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "WebServer" {
  name                 = "WebServer-ASG-${aws_launch_configuration.WebServerLC.name}"
  launch_configuration = aws_launch_configuration.WebServerLC.name
  min_size             = 2
  max_size             = 2
  min_elb_capacity     = 2
  force_delete = true
  vpc_zone_identifier = [
    aws_default_subnet.default_az_1.id,
    aws_default_subnet.default_az_2.id
  ]
  health_check_type = "ELB"
  load_balancers    = [aws_elb.WebServerLB.name]
  dynamic "tag" {
    for_each = merge(var.common_tags, {
      "Name" = "${var.common_tags["Environment"]} WebServer autoscaling group"
    })
    content {
      key                 = tag.key
      value               = tag.value
      propagate_at_launch = true
    }
  }
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_elb" "WebServerLB" {
  name = "WebServer-ELB"
  availability_zones = [
    data.aws_availability_zones.available_zones.names[0],
    data.aws_availability_zones.available_zones.names[1],
  ]
  security_groups = [aws_security_group.WebServerSG.id]
  listener {
    lb_port           = 80
    lb_protocol       = "http"
    instance_port     = 80
    instance_protocol = "http"
  }
  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:80/"
    interval            = 10
  }
  tags = merge(var.common_tags, {
    "Name" = "${var.common_tags["Environment"]} WebServer load balancer"
  })
}

resource "aws_default_subnet" "default_az_1" {
  availability_zone = data.aws_availability_zones.available_zones.names[0]
}
resource "aws_default_subnet" "default_az_2" {
  availability_zone = data.aws_availability_zones.available_zones.names[1]
}
resource "aws_key_pair" "ssh_key" {
  key_name   = "ssh_key"
  public_key = vars.ssh_key
}
