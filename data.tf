data "aws_ami" "amazon_linux_ami" {
  owners      = ["137112412989"]
  most_recent = true
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2"]
  }
}
data "aws_ami" "ubuntu_server_ami" {
  owners      = ["099720109477"]
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images-*ubuntu-*-amd64-server-*"]
  }
  filter{
    name = "virtualization-type"
    values = [ "hvm" ]
  }
}
data "aws_availability_zones" "available_zones" {}
