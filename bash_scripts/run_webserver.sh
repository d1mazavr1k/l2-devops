#!/bin/bash
sudo apt update
sudo apt install -y apache2
sudo ufw allow 'Apache'
myip=$(curl https://ifconfig.me/)
sudo mkdir /var/www/your_domain
sudo chown -R $USER:$USER /var/www/your_domain
sudo echo "
<html><head></head><body bgcolor='black'>
<h1><font color='red'>WebServer with IP: $myip</font></h1>
<h2><font color='gold'>Build by Power of Terraform</font></h2>
<h3><font color='magenta'>Version 2.0</font></h3>
</body></html>
" >/var/www/your_domain/index.html
sudo chown -R $USER:$USER /etc/apache2/sites-available
sudo echo "<VirtualHost *:80>
    ServerAdmin webmaster@localhost
    ServerName your_domain
    ServerAlias www.your_domain
    DocumentRoot /var/www/your_domain
    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>" > /etc/apache2/sites-available/your_domain.conf
sudo a2ensite your_domain.conf
sudo a2dissite 000-default.conf
sudo systemctl restart apache2